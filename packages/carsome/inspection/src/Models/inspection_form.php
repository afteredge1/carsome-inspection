<?php

namespace carsome\inspection\Models;

use Illuminate\Database\Eloquent\Model;

class Inspection_form extends Model
{
    protected $table = 'inspection_forms';
    protected $primaryKey = 'id';
    protected $fillable = [
        'first_name',
        'last_name',
        'address',
        'email',
        'contact_number',
        'date',
        'time'
    ];
}
