<?php

namespace carsome\inspection;

use Illuminate\Support\ServiceProvider;

class InspectionServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        require __DIR__.'/routes.php';
        $this->loadViewsFrom(__DIR__.'/Views', 'inspection');
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {

    }

    public function provides()
    {
        
    }
}

