<?php
namespace carsome\inspection\Services;

use carsome\inspection\Models\Inspection_form;
use carsome\inspection\Contracts\TimeSlotsServiceContract;
use Illuminate\Support\Str;

class TimeSlotsService implements TimeSlotsServiceContract {

    protected $model;
    public function __construct(Inspection_form $model) {
        $this->model = $model;
    }

    public function GetTimeIntervals($date = '') {

        // variable declarations
        $all_intervals = array();
        $start=strtotime('09:00:00');
        $end=strtotime('18:00:00');
        $date = ($date=='') ? date('Y-m-d') : $date;
        // set 4 slots in case of weekend & 2 in week days
        $slots_available = (date('D', strtotime($date))=='Sat') ? 4 : 2;

        // create intervals of 30 mins
        for ($i=$start; $i < $end; $i+=1800) {
            $all_intervals[date('H:i', $i)] = 0;
        }

        // set slots disabled already booked
        foreach ($all_intervals as $key => $value) {

            // fetch already booked slots from DB
            $booked_slots = $this->getByBookingDate($date, $key);
            if (count($booked_slots) >= $slots_available) {
                $all_intervals[$key] = 1;
            }
        }

        return $all_intervals;
    }

    public function getByBookingDate($date,$time) {
        return $this->model
            ->where(array(
                'date' => $date,
                'time' => $time
            ))
            ->select('time')
            ->get();
    }
}