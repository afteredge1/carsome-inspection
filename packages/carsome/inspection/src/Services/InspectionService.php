<?php
namespace carsome\inspection\Services;

use carsome\inspection\Models\Inspection_form;
use carsome\inspection\Contracts\InspectionServiceContract;
use Illuminate\Support\Str;

class InspectionService implements InspectionServiceContract {

    protected $model;
    public function __construct(Inspection_form $model) {
        $this->model = $model;
    }

    public function Add($r) {
        $this->model->first_name = $r->get('first_name');
        $this->model->last_name = $r->get('last_name');
        $this->model->address = $r->get('address');
        $this->model->email = $r->get('email');
        $this->model->contact_number = $r->get('contact_number');
        $this->model->date = $r->get('bookingdate');
        $this->model->time = $r->get('bookingtime');
        $this->model->save();
        return $this->model;
    }

    public function getAll() {
        return $this->model->get();
    }

    public function GetAllbyPagination($page_size) {
        return $this->model
        ->orderBy('created_at')->paginate($page_size);
    }

    public function getById($id) {
        return $this->model->where('id', $id)->first();
    }

    public function getUserAlreadyRegistered($r) {
        return $this->model
            ->where(array(
                'email' => $r->get('email'),
                'date' => $r->get('bookingdate'),
                'time' => $r->get('bookingtime')
            ))
            ->get();
    }
}