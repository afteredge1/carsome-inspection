@extends('layouts.main')

@push('styles')
<style>
    div {
        padding:5px;
    }
    select option:disabled {
        color: red;
    }
</style>    
@endpush

@section('content')
    <div class="col-lg-12">
        <div class="row">
            <div class="col-lg-12">
                <h1>CARSOME - Inspection Booking</h1>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div id="validation-errors"></div>
                <form id="bookingform" name="bookingform" method="POST" action="{{ url('/newbooking')}}">
                    <div class="form-row">
                      <div class="form-group col-md-6">
                        <label for="inputFName4">First name</label>
                        <input type="text" class="form-control" name="first_name" id="inputFName4" placeholder="First name" required>
                        @if($errors->has('first_name'))
                            <div class="error">{{ $errors->first('first_name') }}</div>
                        @endif
                      </div>
                      <div class="form-group col-md-6">
                        <label for="inputLName4">Last name</label>
                        <input type="text" class="form-control" name="last_name" id="inputLName4" placeholder="Last name" required>
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="inputAddress">Address</label>
                      <input type="text" class="form-control" name="address" id="inputAddress" placeholder="1234 Main St" required>
                    </div>
                    <div class="form-group">
                      <label for="inputEmail">Email</label>
                      <input type="email" class="form-control" name="email" id="inputEmail" placeholder="example@domain.com" required>
                    </div>
                    
                    <div class="form-row">
                      <div class="form-group col-md-6">
                        <label for="inputCNumber">Contact number</label>
                        <input type="tel" class="form-control" name="contact_number" id="inputCNumber" placeholder="+60 0000 00 0000" required>                
                      </div>
                      <div class="form-group col-md-4">
                        <label for="inputDate">Date</label>
                        <input type="date" class="form-control" name="bookingdate" id="inputDate" min="{{ $date_from }}" max="{{ $date_to }}" required onchange="gettimeslots();">
                      </div>
                      <div class="form-group col-md-2">
                        <label for="inputTime">Time</label>
                        <select name="bookingtime" id="inputTime" class="form-control" required>
                          <option value="" selected>Choose...</option>
                        </select>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="form-check">
                        <input class="form-check-input" type="checkbox" id="gridCheck">
                        <label class="form-check-label" for="gridCheck">
                        &nbsp; accept terms & conditions
                        </label>
                      </div>
                    </div>
                    @csrf
                    <button type="button" class="btn btn-primary" onclick="createbooking();">Register</button>
                  </form>
            </div>
        </div>
    </div>
@endsection

@push('js')
    <script>

        // code to disable Sundays
        const date_picker = document.getElementById('inputDate');
        date_picker.addEventListener('input', function(e){
            let day = new Date(this.value).getUTCDay();
            if([0].includes(day)){
                e.preventDefault();
                this.value = '';
                alert('Sorry! We are close on Sunday!');
            }
        });

        // create booking function
        function createbooking() {

            // AJAX Call for saving form data
            $.ajax({
            type:"POST",//GET or POST
            url:'newbooking',
            data: $('#bookingform').serialize(),
            'global': false,
                success:function(result){
                    
                    let boxresult = JSON.parse(result.data);

                    if (boxresult=='exist') {
                        $('#validation-errors').html('');
                        $('#validation-errors').append('<div class="alert alert-danger">Your booking for same date/time already exists.</div>');
                    } else {
                        $('#validation-errors').html('');
                        $('#validation-errors').append('<div class="alert alert-success">Thank you! Your booking is created.</div>');
                        location.reload(true);
                    }

                }, error: function (request, status, error) {
                    console.log(request.responseText);
                    $('#validation-errors').html('');
                    $.each(request.responseJSON.errors, function(key,value) {
                        $('#validation-errors').append('<div class="alert alert-danger">'+value+'</div>');
                    }); 
                }
            });
        }

        // fill timeslots listbox
        function gettimeslots() {

            const date_picker = $('#inputDate').val();
                        
            // AJAX Call for time intervals
            $.ajax({
            type:"POST",//GET or POST
            url:'timeintervalsbydate',
            data: {
                'date': date_picker,
                "_token": '{{ csrf_token() }}'
            },
            'global': false,
                success:function(result){
                    
                    let boxresult = JSON.parse(result.data);
                    // empty existing values in listbox
                    $('#inputTime').empty();
                    $('#inputTime').append('<option value="">Choose...</option>');
                    $.each(boxresult, function(key,value) {
                        let disable = (value==1) ? 'disabled' : '';
                        $('#inputTime').append('<option value='+ key +' '+ disable +'>'+ key +'</option>');
                    });

                }, error: function (request, status, error) {
                    console.log(request.responseText);
                }
            });
        }
    </script>
@endpush