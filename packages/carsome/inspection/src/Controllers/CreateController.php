<?php

namespace carsome\inspection\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use carsome\inspection\Requests\InspectionRequest;
use carsome\inspection\Services\InspectionService;
use carsome\inspection\Services\TimeSlotsService;

class CreateController extends Controller
{
    protected $inspectionService;
    protected $timeSlotsService;
    public function __construct(InspectionService $inspectionService,
        TimeSlotsService $timeSlotsService) {

        $this->inspectionService = $inspectionService;
        $this->timeSlotsService = $timeSlotsService;
    }
    
    public function Post(InspectionRequest $r) {
        // check if user already registered for same slot
        $existing = $this->inspectionService->getUserAlreadyRegistered($r);
        if (count($existing) > 0) {
            return response()->json([
                'data' => json_encode('exist')
            ]);
        }
        // create user booking
        $result = $this->inspectionService->Add($r);

        if($result->id) {
            return response()->json([
                'data' => json_encode('success')
            ]);
        }
    }

    public function GetTimeIntervals(Request $r) {

        $date = $r->get('date');
        $time_intervals = '';
        
        // get time intervals
        $result = $this->timeSlotsService->GetTimeIntervals($date);
                
        return response()->json([
            'data' => json_encode($result)
        ]);
    }
}
