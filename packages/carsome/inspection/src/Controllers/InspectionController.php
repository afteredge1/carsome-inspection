<?php

namespace carsome\inspection\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use carsome\inspection\Services\InspectionService;
use carsome\inspection\Services\TimeSlotsService;
use Config;

class InspectionController extends Controller
{
    protected $inspectionService;
    public function __construct(InspectionService $inspectionService,
        TimeSlotsService $timeSlotsService) {

        $this->inspectionService = $inspectionService;
        $this->timeSlotsService = $timeSlotsService;
    }

    public function Index() {

        // set 3 week date interval
        $date_from = date('Y-m-d');
        $date_to = date('Y-m-d', strtotime('+3 weeks'));
        
        $data = array(
            'date_from' => $date_from,
            'date_to' => $date_to
        );

        return response()->view('inspection::create', $data);
    }
}
