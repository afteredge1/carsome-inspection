<?php
namespace carsome\inspection\Contracts;

interface InspectionServiceContract {
    public function Add($r);
    public function getAll();
    public function GetAllbyPagination($page_size);
    public function getById($id);
    public function getUserAlreadyRegistered($r);
}