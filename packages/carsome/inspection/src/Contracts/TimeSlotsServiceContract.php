<?php
namespace carsome\inspection\Contracts;

interface TimeSlotsServiceContract {
    public function GetTimeIntervals($date);
    public function getByBookingDate($date,$time);
}