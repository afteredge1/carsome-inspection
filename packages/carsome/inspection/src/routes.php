<?php

Route::group(['middleware' => ['web']], function() {
    // home page
    Route::get('/home', 'carsome\inspection\Controllers\InspectionController@Index');
    
    // new booking
    Route::post('/newbooking', 'carsome\inspection\Controllers\CreateController@Post');
    
    // time intervals
    Route::post('/timeintervalsbydate', 'carsome\inspection\Controllers\CreateController@GetTimeIntervals');
});