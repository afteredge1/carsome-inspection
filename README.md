## Car Inspection

Car owner will come to web portal to book an inspection slot. We need to come up
with a booking module to handle this.

# Operation Rules:

1. Portal will have 2 inspection slot every 30mins, from 9AM to 6PM on weekdays
2. Portal will have 4 inspection slot every 30mins, from 9AM to 6PM on Saturday
3. Portal will rest on Sunday

# Deliverable:

1. Frontend form
2. Backend API to handle request

# Requirements:

1. Users cannot book the inspection slot in the same hour.
2. Users can only book the inspection slot for the next 3 weeks.

# For developed module

refer to folder:
Packages/carsome/inspection/
